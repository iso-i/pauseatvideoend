#!/bin/sh

KEY="isobel_chrome_key"
openssl genrsa 2048 | openssl pkcs8 -topk8 -nocrypt -out ${KEY}.pem
openssl rsa -in ${KEY}.pem -pubout -outform DER | openssl base64 -A 2>&1 1> ${KEY}.der
openssl rsa -in ${KEY}.pem -pubout -outform DER | shasum -a 256 | head -c32 | tr 0-9a-f a-p 2>&1 1> ${KEY}.fingerprint
