I just want the video to pause at the end so that I can read the comments and Like/dislike it, but YouTube "ain't got no time for that" and wants to rush off to the next video in the playlist.

This tries (until the inevitable site redesign) to pause the video at the end. It has to:

Work out how long the video is
Work out where we are at playback
Pause the video at the top of the last second.

[Toggle "Autoplay" off]

THAT IS ALL.
